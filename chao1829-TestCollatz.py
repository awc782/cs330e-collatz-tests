#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read3(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 10)

    def test_read4(self):
        self.assertRaises(IndexError, collatz_read, "12")


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(12, 2)
        self.assertEqual(v, 20)

    def test_eval_3(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    def test_eval_4(self):
        v = collatz_eval(1, 100)
        self.assertEqual(v, 119)
        
    def test_eval_5(self):
        v = collatz_eval(999992, 999999)
        self.assertEqual(v, 259)

    def test_eval_6(self):
        v = collatz_eval(2400, 4002)
        self.assertEqual(v, 238)

    def test_eval_7(self):
        v = collatz_eval(6200, 7900)
        self.assertEqual(v, 239)
        

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        self.assertRaises(TypeError, collatz_print, 1, 10, 20)

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 12, 2, 5)
        self.assertEqual(w.getvalue(), "12 2 5\n")
                          

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("2 4\n12 2\n200 300\n")
        w = StringIO()
        write = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "2 4 8\n12 2 20\n200 300 128\n") 

    def test_solve3(self):
        r = StringIO("12")
        w = StringIO()
        self.assertRaises(IndexError, collatz_solve, r, w)

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
